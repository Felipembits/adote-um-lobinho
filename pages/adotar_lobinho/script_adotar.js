function substituirparalobo(img, name, id){
    let imagem = document.querySelector(".imgLobo");
    let nomelobo = document.querySelector(".nomedolobo")
    let idlobo = document.querySelector(".numid")
    imagem.setAttribute("src", img)
    nomelobo.innerText = "Adote o(a) "+ name;
    idlobo.innerText = "Id: " + id
}

let url = new URLSearchParams(window.location.search)
let id = url.get("id")



function getLobos(id){
    const urlJson = "../../JSON/lobinhos.json"
    const fetchConfig = {
        "method":"GET"
        
    };
    fetch(urlJson, fetchConfig)
        .then((resposta) => {
            resposta.json()
                .then((resposta) => {
                    console.log()
                    substituirparalobo(resposta[id].imagem, resposta[id].nome, id);
                })
                .catch((error) =>{
                    console.log(error);
                })
        })
        .catch((error)=> {
            console.log(error);
        })
}
getLobos(id);

let btn_adota = document.querySelector(".btn_adota")
btn_adota.addEventListener("click", ()=>{
    alert("Lobinho adotado com sucesso!🐺")
})
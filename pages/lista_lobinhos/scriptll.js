const urlJson = "../../JSON/lobinhos.json"

function createLobo(loboNome, loboIdade, loboDescricao, loboImagem, loboId, adotado, donoLobo){
    let section = document.createElement(`section`);
    section.classList.add('secao');
    
    let figure = document.createElement('figure');
    
    let caixa = document.createElement('div');
    caixa.classList.add('caixa');
    
    let image = document.createElement('img');
    image.classList.add('imgLobo');
    image.setAttribute('src', loboImagem);
    caixa.append(image);
    figure.append(caixa);

    let infoLobo = document.createElement('div');
    infoLobo.classList.add('textoebtn');

    let infoHeader = document.createElement('div');
    infoHeader.classList.add('txtbtnsub');

    let nomeIdade = document.createElement('div');
    nomeIdade.classList.add('nomeidade');

    let nome = document.createElement('h2');
    nome.classList.add('nome');
    nome.innerText = loboNome;
    nomeIdade.append(nome);
    

    let idade = document.createElement('idade');
    idade.classList.add('idade');
    idade.innerText = `Idade: ${loboIdade} anos`;
    nomeIdade.append(idade);
    infoHeader.append(nomeIdade);

   
    
    
    if(adotado){
        
        let adotado = document.createElement('button');
        adotado.classList.add('btnadotado');
        let direcionamento2 = document.createElement("a")
        direcionamento2.setAttribute("href", `../show_lobinhos/show_lobinho.html?id=${loboId}`);
        direcionamento2.innerText = "Adotado";
        adotado.append(direcionamento2);
        let texto = document.createElement('p');
        texto.classList.add('texto');
        texto.innerText = loboDescricao;
        let dono = document.createElement('p');
        let textoDono = document.createElement('div');
        dono.classList.add('donoLobo');
        dono.innerText = `Adotado por: ${donoLobo}`;
        textoDono.append(texto);
        textoDono.append(dono);
        infoLobo.append(infoHeader);
        infoLobo.append(textoDono);
        infoHeader.append(adotado);
    }
    else{
        let adotar = document.createElement('button');
        let direcionamento = document.createElement("a")
        direcionamento.setAttribute("href", `../show_lobinhos/show_lobinho.html?id=${loboId}`);
        infoHeader.append(adotar);
        infoLobo.append(infoHeader);
        direcionamento.innerText = "Adotar"
        adotar.append(direcionamento)
    
        let texto = document.createElement('p');
        texto.classList.add('texto');
        texto.innerText = loboDescricao;
        infoLobo.append(texto);
    }
    section.append(figure);
    section.append(infoLobo);
    section.setAttribute("id", loboId);
    let mainLobo = document.querySelector("#main");
    mainLobo.append(section);

    rotatePictures(section, loboId);
}

function rotatePictures(section, loboId){
    if(loboId % 2 !== 0){
        section.style.flexDirection = "row-reverse";
    }
}

function getLobos(adopted=false){
    const fetchConfig = {
        "method":"GET"
    };
    fetch(urlJson, fetchConfig)
        .then((resposta) => {
            resposta.json()
                .then((resposta) => {
                    //Versão que puxa apenas 5 lobos do JSON por questões de desempenho do site;
                    let count = 5;
                    let wolf = 0;
                    let i = 0;
                    while(wolf < count){
                        if(adopted){
                            if(!resposta[i].adotado){
                                i++;
                                continue;
                            }
                            createLobo(resposta[i].nome, resposta[i].idade, resposta[i].descricao, resposta[i].imagem, i, resposta[i].adotado, resposta[i].nomeDono);
                            wolf++;
                        }
                        else{
                            if(adopted == resposta[i].adotado){
                            createLobo(resposta[i].nome, resposta[i].idade, resposta[i].descricao, resposta[i].imagem, i, resposta[i].adotado, resposta[i].nomeDono);
                            wolf++;
                            }
                        }
                        i++;
                    };
                    // Versão para puxar o JSON inteiro
                    //     resposta.map((resposta, i) => {
                    //         let count = 0;
                    //         if(adopted){
                    //             if(!resposta.adotado){
                    //                 return null;
                    //             }
                    //             createLobo(resposta.nome, resposta.idade, resposta.descricao, resposta.imagem, resposta.id, resposta.adotado, resposta.nomeDono);
                    //                 count++;
                    //         }
                    //         else{
                    //             createLobo(resposta.nome, resposta.idade, resposta.descricao, resposta.imagem, resposta.id, resposta.adotado, resposta.nomeDono);
                    //                 count++;
                    //         }
                    // });
                })
                .catch((error) =>{
                    console.log(error);
                })
        })
        .catch((error)=> {
            console.log(error);
        })
}

let filter = document.querySelector("#lobosAdotados");

getLobos()

filter.addEventListener('change', () => {
    if(filter.checked){
        document.querySelectorAll('.secao').forEach(e => e.remove());
        getLobos(true);
    } else{
        document.querySelectorAll('.secao').forEach(e => e.remove());
        getLobos();
    }
});

function pesquisalobo(){
    var input = document.querySelector("#pesquisa").value;
    input = input.toUpperCase().trim();
    const fetchConfig = {
        "method":"GET"
    }
    fetch(urlJson, fetchConfig)
        .then((resposta) => {
            console.log(resposta)
        resposta.json()
            .then((resposta) => {
                let count = 20;
                let i = 0;
                while(i < count){                    
                    let a = resposta[i].nome.toUpperCase();

                    if(input == a){
                        createLobo(resposta[i].nome, resposta[i].idade, resposta[i].descricao, resposta[i].imagem, i, resposta[i].adotado, resposta[i].nomeDono);
                        
                        i++
                        
                    }
                    else{
                        i++
                        continue
                        
                    }
                };
            })
            .catch((error) =>{
                console.log(error);
            })
    })
    .catch((error)=> {
        console.log(error);
    })
}
let testebtn = document.querySelector(".teste")
testebtn.addEventListener("click", () => document.querySelectorAll('.secao').forEach(e => e.remove()));
testebtn.addEventListener("click", () => pesquisalobo())

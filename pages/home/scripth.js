const urlJson = "../../JSON/lobinhos.json"

function createLobo(loboNome, loboIdade, loboDescricao, loboImagem, loboId){
    let section = document.createElement(`section`);
    section.classList.add('secao');
    
    let figure = document.createElement('figure');
    
    let caixa = document.createElement('div');
    caixa.classList.add('caixa');
    
    let image = document.createElement('img');
    image.classList.add('imgLobo');
    image.setAttribute('src', loboImagem);
    caixa.append(image);
    figure.append(caixa);

    let infoLobo = document.createElement('div');
    infoLobo.classList.add('textoebtn');

    let infoHeader = document.createElement('div');
    infoHeader.classList.add('txtbtnsub');

    let nomeIdade = document.createElement('div');
    nomeIdade.classList.add('nomeidade');

    let nome = document.createElement('h2');
    nome.classList.add('nome');
    nome.innerText = loboNome;
    nomeIdade.append(nome);

    let idade = document.createElement('idade');
    idade.classList.add('idade');
    idade.innerHTML = loboIdade;
    nomeIdade.append(idade);
    infoHeader.append(nomeIdade);

    let adotar = document.createElement('button');
    let direcionamento = document.createElement("a")
    direcionamento.setAttribute("href", `../show_lobinhos/show_lobinho.html?id=${loboId}`);
    infoHeader.append(adotar);
    infoLobo.append(infoHeader);
    direcionamento.innerText = "Adotar"
    adotar.append(direcionamento)

    let texto = document.createElement('p');
    texto.classList.add('texto');
    texto.innerText = loboDescricao;
    infoLobo.append(texto);
    section.append(figure);
    section.append(infoLobo);

    let mainLobo = document.querySelector("#api");
    console.log(mainLobo);
    mainLobo.append(section);
}

function getLobos(){
    const fetchConfig = {
        "method":"GET"
    };

    fetch(urlJson, fetchConfig)
        .then((resposta) => {
            resposta.json()
                .then((resposta) => {
                    let aleatorio = Math.floor(Math.random() * 1000);
                    createLobo(resposta[aleatorio].nome, resposta[aleatorio].idade, resposta[aleatorio].descricao, resposta[aleatorio].imagem, aleatorio);
                    let aleatorio2 = Math.floor(Math.random() * 1000);
                    createLobo(resposta[aleatorio2].nome, resposta[aleatorio2].idade, resposta[aleatorio2].descricao, resposta[aleatorio2].imagem, aleatorio2);
                    
                })
                .catch((error) =>{
                    console.log(error);
                })
        })
        .catch((error)=> {
            console.log(error);
        })
}
getLobos();

function getLobos(id){
    const urlJson = "../../JSON/lobinhos.json"
    const fetchConfig = {
        "method":"GET"
        
    };
    fetch(urlJson, fetchConfig)
        .then((resposta) => {
            resposta.json()
                .then((resposta) => {
                    console.log()
                    createLobo(resposta[id].nome, resposta[id].descricao, resposta[id].imagem);
                })
                .catch((error) =>{
                    console.log(error);
                })
        })
        .catch((error)=> {
            console.log(error);
        })
}

function createLobo(nome, descricao,imagem){
    let image = document.querySelector(".imgLobo")
    let lobo = document.querySelector(".nomelobo")
    let descricao_lobo = document.querySelector(".texto")

    image.setAttribute("src",imagem);
    lobo.innerText = nome;
    descricao_lobo.innerText = descricao;  
}





let url = new URLSearchParams(window.location.search)
let id = url.get("id")
getLobos(id);
let btn_adota = document.querySelector(".adotar");
let direcionamento = document.createElement("a")
btn_adota.append(direcionamento)
direcionamento.innerText = "Adotar"
direcionamento.setAttribute("href",`../adotar_lobinho/adotar_lobinho.html?id=${id}`)
